let validateForm = () => {
    const isFormNameValid = formNameValdate();
    const isFormGradeValid = formGradeValidate();
    return isFormNameValid && isFormGradeValid;
};

let formNameValdate = function () {
    const emptyFieldErrorMessage = 'Вы забыли указать имя и фамилию';
    const minCharacterCountErrorMessage = 'Имя не может быть короче 2-х символов';

    let formNameErrorElement = document.getElementById('form-grade-error');
    formNameErrorElement.hidden = true;

    let formNameInput = document.getElementById('form-name');
    let formNameValue = formNameInput.value;

    if (!formNameValue) {
        showFormNameError(emptyFieldErrorMessage);
        return false;
    }

    if (formNameValue.length < 2) {
        showFormNameError(minCharacterCountErrorMessage);
        return false;
    }

    hideFormNameError();
    return true;
};

let showFormNameError = function (errorMessage) {
    let formNameErrorElement = document.getElementById('form-name-error');
    formNameErrorElement.innerText = errorMessage;
    formNameErrorElement.hidden = false;
}

let hideFormNameError = function () {
    let formNameErrorElement = document.getElementById('form-name-error');
    formNameErrorElement.hidden = true;
}

let formGradeValidate = function() {
    const invalidGradeValueErrorMessage = 'Оценка должна быть от 1 до 5';

    let formGradeInput = document.getElementById('form-grade');
    let formGradeValue = formGradeInput.value;

    if (
        !formGradeValue
        || !Number.isFinite(+formGradeValue)
        || (1 > formGradeValue || 5 < formGradeValue)
    ) {
        showformGradeError(invalidGradeValueErrorMessage);
        return false;
    }
    hideformGradeError();
    return true;
}

let showformGradeError = function(errorMessage) {
    let formGradeErrorElement = document.getElementById('form-grade-error');
    formGradeErrorElement.innerText = errorMessage;
    formGradeErrorElement.hidden = false;
}

let hideformGradeError = function() {
    let formGradeErrorElement = document.getElementById('form-grade-error');
    formGradeErrorElement.hidden = true;
}

