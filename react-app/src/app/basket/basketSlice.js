import {createSlice} from "@reduxjs/toolkit";

export const basketSlice = createSlice({
    name: "basket",
    initialState: {
        products: [],
    },
    reducers: {
        addProduct: (prevState, action) => {
            return {
                ...prevState,
                products: [...prevState.products, action.payload],
            }
        },
        removeProduct: (prevState, action) => {
            return {
                ...prevState,
                products: prevState.products.filter(product => product !== action.payload),
            }
        },
    }
});

export const { addProduct, removeProduct } = basketSlice.actions;
export const selectProducts = (state) => state.basket.products;
export default basketSlice.reducer;