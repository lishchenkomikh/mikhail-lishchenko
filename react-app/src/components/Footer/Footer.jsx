import './Footer.css';
import Link from "../Link/Link";

function Footer() {

  return (
      <footer className="footer align-center">
          <div>
              Ⓒ ООО "
              <span className="market-title">Мой </span>Маркет", 2018-2020.
              <br /> Для уточнения по информации звоните по номеру
              <Link href={'tel:+79000000000'} label={' +7 900 000 0000'}/>,
              <br /> а предложения по сотрудничеству отправляйте на почту
              <Link href={'mailto:partner@mymarket.com'} label={' partner@mymarket.com'}/>
          </div>
          <Link href={'#'} label={'Наверх'}/>
      </footer>
  );
}

export default Footer;