import './Link.css';

function Link(props) {
    return (
        <span>
            <a className="a" href={props.href}>{props.label}</a>
        </span>
    );
}

export default Link;