import './First-page.css';
import Link from "../Link/Link";

function FirstPage() {
    return (
        <div className="flex-box flex-column flex-center align-center first-box">
            <div className="page-text">
                Здесь должно быть содержимое главной страницы.
                Но в рамках курса главная страница используется лишь
                в демонстрационных целях
            </div>
            <Link href={'/product'} label={'Перейти на страницу товара'}/>
        </div>
    );
}

export default FirstPage;