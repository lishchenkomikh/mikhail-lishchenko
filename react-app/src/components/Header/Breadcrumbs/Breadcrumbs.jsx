import './Breadcrumbs.css';
import Link from "../../Link/Link";

function Breadcrumbs() {
    const items = [
        {
            href: 'file:///C:/Frontstart/mikhail-lishchenko/index.html',
            label: 'Электроника'
        },
        {
            href: 'file:///C:/Frontstart/mikhail-lishchenko/index.html',
            label: 'Смартфоны и гаджеты'
        },
        {
            href: 'file:///C:/Frontstart/mikhail-lishchenko/index.html',
            label: 'Мобильнык телефоны'
        },
        {
            href: 'file:///C:/Frontstart/mikhail-lishchenko/index.html',
            label: 'Apple'
        },
    ];
        return (
        <nav className="nav">
            {items.map((item) => (
                <span key={item.label}>
                    <Link href={item.href} label={item.label}/>>
                </span>
            ))}
        </nav>
    );
}

export default Breadcrumbs;