import './Header.css';
import favicon from '../../images/favicon.ico';
import Breadcrumbs from "./Breadcrumbs/Breadcrumbs";
import BasketCounter from "./Basket-counter/Basket-counter";

function Header() {

  return (
      <header className="header">
          <div className="fixed-head flex-box space-between">
              <div className="market">
                  <h1 className="h1"><img src={favicon} alt="logo" />
                      <a className="a-main" href={'/'}><span className="market-title">Мой </span>Маркет</a></h1>
              </div>
              <BasketCounter/>
          </div>
          <Breadcrumbs/>
      </header>
  );
}

export default Header;