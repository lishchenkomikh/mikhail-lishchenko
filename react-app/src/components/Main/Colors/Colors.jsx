import './Colors.css';
import {useState} from "react";

function Colors() {
    const [selectedColor, setColor] = useState('blue');
    const colors = ['red', 'green', 'pink', 'blue', 'white', 'black'];
    const buttonClass = "button mini-phone align-center";

    const selectColor = (colorName) => {
        setColor(colorName);
    }
    return (
        <div>
            <h3>Цвет товара: синий</h3>
            <div className="flex-box">
            {colors.map((color) => (
                    <button key={color} className={color === selectedColor ? `${buttonClass} mini-phone-active` : buttonClass}
                            onClick={() => selectColor(color)}>
                        <div className="img-block">
                            <div className={`img ${color}`}></div>
                        </div>
                    </button>
            ))}
            </div>
        </div>
     );
}

export default Colors;