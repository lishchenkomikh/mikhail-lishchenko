import React from 'react';
import './Main.css';
import MainPhoto from './Main-photo/Main-photo';
import Colors from './Colors/Colors';
import Memory from './Memory/Memory';
import Characteristics from './Сharacteristics/Сharacteristics';
import Description from './Description/Description';
import Models from './Models/Models';
import Feedback from './Feedback/Feedback';
import UserActions from './User-Actions/User-Actions';

function Main(props) {
    return (
        <main className="main">
            <MainPhoto/>
            <Colors/>
            <div className="main-container">
                <div className="main-content left-content">
                    <Memory/>
                    <Characteristics/>
                    <Description/>
                    <Models/>
                    <Feedback/>
                </div>
                <div className="main-content right-menu">
                    <UserActions/>
                    <div className="frame-box flex-box flex-column">
                        <span className="frame-title">Реклама</span>
                    </div>
                </div>
            </div>
        </main>
    );
}

export default Main;