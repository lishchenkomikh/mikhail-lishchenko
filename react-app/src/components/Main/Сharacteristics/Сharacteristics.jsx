import './Characteristics.css';

function Characteristics() {
    return ( 
        <div>
        <h3 className="h3">Характеристики товара</h3>
        <ul className="ul">
            <li>Экран: <b>6.1</b></li>
            <li>Встроенная память: <b>128 ГБ</b></li>
            <li>Операционная система: <b>iOS 15</b></li>
            <li>Беспроводные интерфейсы: <b>NFC, Bluetooth, Wi-Fi</b></li>
            <li>Процессор:<a className="a" href="https://ru.wikipedia.org/wiki/Apple_A15" target="_blank">Apple A15
                BiOnic</a>
            </li>
            <li>Вес: <b>173 г</b></li>
        </ul>
    </div>
     );
}

export default Characteristics;