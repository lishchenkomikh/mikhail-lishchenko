import './Memory.css';
import {useState} from "react";

function Memory() {
    const memories = ['128 ГБ', '256 ГБ', '512 ГБ'];
    const [selectedMemory, setMemory] = useState('128 ГБ');
    const buttonClass = "button memory-button";

    const selectMemory = (memoryName) => {
        setMemory(memoryName);
    }
    return ( 
        <div>
        <h3>Конфигурация памяти: 128 ГБ</h3>
        <div className="flex-box">
            {memories.map((memory) => (
                <button key={memory} className={memory === selectedMemory ? `${buttonClass} memory-button-active` : buttonClass}
                        onClick={() => selectMemory(memory)}>{memory}</button>
            ))}
        </div>
    </div>
     );
}

export default Memory;