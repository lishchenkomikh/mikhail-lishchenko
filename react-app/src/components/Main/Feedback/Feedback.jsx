import './Feedback.css';
import review1 from '../../../images/review-1.jpeg';
import review2 from '../../../images/review-2.jpeg';
import star4 from '../../../images/star-4.png';
import star5 from '../../../images/star-5.png';
import {useState} from "react";

function Feedback() {
    const [feedbacks, setFeedbacks] = useState([
        {
            img: review1,
            name: 'Марк Г.',
            rate: star5,
            experience: 'менее месяца',
            advantage: `Это мой первый айфон после после огромного количества телефонов на андроиде,все
                            плавно,четко и
                            красиво.довольно
                            шустрое устройство. камера весьма не плохая, ширик тоже на высоте.`,
            disadvantage: `к самому устройству мало имеет отношения, но перенос данных с андроида - адская вещь) а
                            если нужно
                            преносить фото с
                            коипа , то это только через itunes,который урезает качество фотографий исходное`,
        },
        {
            img: review2,
            name: 'Валерий Коваленко',
            rate: star4,
            experience: 'менее месяца',
            advantage: 'OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго',
            disadvantage: 'Плохая ремонтопригодность',
        }
    ]);
    const [showNameError, setShowNameError] = useState(false);
    const [nameError, setNameError] = useState('');
    const [showGradeError, setShowGradeError] = useState(false);
    const [gradeError, setGradeError] = useState('');
    const [name, setName] = useState('');
    const [rate, setRate] = useState('');
    const [advantage, setAdvantage] = useState('');

    const isFormValid = () => {
        const isFormNameValid = formNameValidate(name);
        const isFormGradeValid = formGradeValidate(rate);
        return isFormNameValid && isFormGradeValid;
    };

    const formNameValidate = (formNameValue) =>  {
        const emptyFieldErrorMessage = 'Вы забыли указать имя и фамилию';
        const minCharacterCountErrorMessage = 'Имя не может быть короче 2-х символов';

        if (!formNameValue) {
            showFormNameError(emptyFieldErrorMessage);
            return false;
        }

        if (formNameValue.length < 2) {
            showFormNameError(minCharacterCountErrorMessage);
            return false;
        }

        hideFormNameError();
        return true;
    };

    const showFormNameError = (errorMessage) => {
        setShowNameError(true);
        setNameError(errorMessage);
    }

    const hideFormNameError = () => {
        setShowNameError(false);
        setNameError('');
    }

    const formGradeValidate = (formGradeValue) => {
        const invalidGradeValueErrorMessage = 'Оценка должна быть от 1 до 5';

        if (
            !formGradeValue
            || !Number.isFinite(+formGradeValue)
            || (1 > formGradeValue || 5 < formGradeValue)
        ) {
            showFormGradeError(invalidGradeValueErrorMessage);
            return false;
        }
        hideFormNameError();
        return true;
    }

    const showFormGradeError = (errorMessage) => {
        setShowGradeError(true);
        setGradeError(errorMessage);
    }

    const hideFormGradeError = () => {
        setShowGradeError(false);
        setGradeError('');
    }

    const changeNameHandler = (event) => {
        setName(event.target.value);
        hideFormNameError();
    }

    const changeRateHandler = (event) => {
        setRate(event.target.value);
        hideFormGradeError();
    }

    const addFeedback = (event) => {
        event.preventDefault();
        if (!isFormValid()) {
            return;
        }
        const feedback = {
            img: review1,
            name: name,
            rate: star5,
            experience: 'менее месяца',
            advantage: advantage,
            disadvantage: 'нет',
        }
        setFeedbacks(prevFeedback => [...prevFeedback, feedback]);
        setName('');
        setRate('');
        setAdvantage('');
    }
    return (
        <div>
            <div className="feedback-title">
                <h3 className="feedback-h3">Отзывы <span className="feedback-h3">425 </span> </h3>
            </div>
            {feedbacks.map((feedback, index) => (
                <div key={feedback.name}>
                    <div className="feedback-block">
                        <img src={feedback.img} className="photo round" alt="альтернативный текст"/>
                        <div className="feedback-text">
                            <p className="p"><b>{feedback.name}</b></p>
                            <img src={feedback.rate} className="rate" alt="альтернативный текст"/>
                            <p className="p"><b>Опыт использования: </b>{feedback.experience}</p>
                            <p className="p"><b>Достоинства:</b>
                                <br/>
                                {feedback.advantage}
                            </p>
                            <p className="p"><b>Недостатки:</b>
                                <br/>
                                {feedback.disadvantage}
                            </p>
                        </div>
                    </div>
                    <hr className={index === feedbacks.length - 1 ? 'hr hidden' : 'hr'}/>
                </div>
            ))}
            <form onSubmit={addFeedback} className="form">
                <div className="new-feedback feedback-text">
                    <h3 className="feedback-legend">Добавить свой отзыв</h3>
                    <div className="flex-box text-input-block space-between">
                        <div className="flex-box flex-column name">
                            <input onChange={changeNameHandler} className="text-input" type="text"
                                   placeholder="Имя и Фамилия"/>
                            {showNameError === true &&
                                <div className="error name-error">{nameError}</div>
                            }
                        </div>
                        <div className="flex-box flex-column number">
                            <input onChange={changeRateHandler} className="text-input"
                                   placeholder="Оценка"/>
                            {showGradeError === true &&
                                <div className="error">{gradeError}</div>
                            }
                        </div>
                    </div>
                    <textarea onChange={(e) => setAdvantage(e.target.value)} className="textarea text-input" placeholder="Текст отзыва"
                              rows="8"></textarea>
                    <button className="submit-button" type="submit">Отправить отзыв</button>
                </div>
            </form>
        </div>
    );
}

export default Feedback;