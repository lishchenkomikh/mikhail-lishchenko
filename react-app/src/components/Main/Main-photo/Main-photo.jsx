import './Main-photo.css';
import image1 from '../../../images/image-1.webp';
import image2 from '../../../images/image-2.webp';
import image3 from '../../../images/image-3.webp';
import image4 from '../../../images/image-4.webp';
import image5 from '../../../images/image-5.webp';

function MainPhoto() {

  return (
      <div>
          <h2 className="h2 h2-title">Смартфон Apple iPhone 13, синий</h2>
          <div className="phones">
              <img src={image1} alt="front and back view" />
              <img src={image2} alt="front view" />
              <img src={image3} alt="side view" />
              <img src={image4} alt="camera view" />
              <img src={image5} alt="back and front view" />
          </div>
      </div>
  );
}

export default MainPhoto;