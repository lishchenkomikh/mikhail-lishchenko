import './Models.css';

function Models() {
    const models = [
        {
            model: 'Iphone11',
            weight: '194 грамма',
            height: '150.9 мм',
            width: '75.7 мм',
            thickness: '8.3 мм',
            chip: 'A13 Bionicchip',
            memory: 'до 128 Гб',
            accumulator: 'До 17 часов',
        },
        {
            model: 'Iphone12',
            weight: '164 грамма',
            height: '146.7 мм',
            width: '71.5 мм',
            thickness: '7.4 мм',
            chip: 'A14 Bionicchip',
            memory: 'до 256 Гб',
            accumulator: 'до 19 часов',
        },
        {
            model: 'Iphone13',
            weight: '174 грамма',
            height: '146.7 мм',
            width: '71.5 мм',
            thickness: '7.65 мм',
            chip: 'A15 Bionicchip',
            memory: 'до 512 Гб',
            accumulator: 'до 19 часов',
        }
    ];
    return (
        <div className="models">
            <h3 className="h3">Сравнение моделей</h3>
            <table className="table">
                <thead className="table-border">
                <tr>
                    <th>Модель</th>
                    <th>Вес</th>
                    <th>Высота</th>
                    <th>Ширина</th>
                    <th>Толщина</th>
                    <th>Чип</th>
                    <th>Обьем памяти</th>
                    <th>Аккумулятор</th>
                </tr>
                </thead>
                <tbody>
                {models.map((model) => (
                    <tr key={model.model}>
                        <td>{model.model}</td>
                        <td>{model.weight}</td>
                        <td>{model.height}</td>
                        <td>{model.width}</td>
                        <td>{model.thickness}</td>
                        <td>{model.chip}</td>
                        <td>{model.memory}</td>
                        <td>{model.accumulator}</td>
                    </tr>
                ))}
                </tbody>
            </table>
        </div>
     );
}

export default Models;