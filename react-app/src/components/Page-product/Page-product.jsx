import React from 'react';
import './Page-product.css';
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import Main from "../Main/Main";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import FirstPage from "../First-page/First-page";

function PageProduct(props) {
    return (
        <div className="body">
            <Header />
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<FirstPage />}/>
                    <Route path="/product" element={<Main />}/>
                </Routes>
            </BrowserRouter>
            <Footer />
        </div>
    );
}

export default PageProduct;