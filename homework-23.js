// Упражнение 1
let count = prompt('Введите число');
let intervalId = setInterval(()=> {
  count = count-1;
  console.log(`Осталось ${count}`);
  if(count === 0) {
    clearInterval(intervalId);
    console.log('Время вышло!');
  }
},1000);

// Упражнение 2
let getUsers = fetch("https://reqres.in/api/users");
getUsers.then(response => response.json()).then(jsonResonse => {
  let users = jsonResonse.data;
  console.log(`Получили пользователей: ${users.length}`);
  users.forEach((item) => console.log(`-${item.first_name} ${item.last_name} (${item.email})`));
}).catch((error) => console.log(`Кажется бэкенд сломался :${error}`));

