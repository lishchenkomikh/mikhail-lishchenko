const basketName = 'basket';
const itemIsNotOnTheBasketText = 'Добавить в корзину';
const itemIsOnTheBasketText = 'Товар уже в корзине';

const itemId = 'iphone_13_1';

function getBasketCounter() {
    return document.getElementById('counter-basket');
}

function getBasketButton() {
    return document.getElementById('basket-button');
}

function getBasketButtonSpan() {
    return document.getElementById('basket-button-span');
}

function getBasketItems() {
    const items = localStorage.getItem(basketName);
    return items ? JSON.parse(items) : [];
}

function setBasketItems(items) {
    if (!items || items.length === 0) {
        localStorage.removeItem(basketName);
        return;
    }
    localStorage.setItem(basketName, JSON.stringify(items));
}

function addToBasket(id) {
    const items = getBasketItems();
    if (!items.includes(id)) {
        items.push(id);
        setBasketItems(items);
    }
    setCounter();
    setBasketButton(id);
}

function removeFromBasket(id) {
    const items = getBasketItems();
    if (items.length === 0 || !items.includes(id)) {
        return;
    }
    const index = items.indexOf(id);
    items.splice(index, 1);
    setBasketItems(items);
    setCounter();
    setBasketButton();
}

function setBasketButton(id) {
    const basketItems = getBasketItems();
    const button = getBasketButton();
    const buttonSpan = getBasketButtonSpan();
    if (basketItems.length === 0 || !basketItems.includes(id)) {
        buttonSpan.textContent = itemIsNotOnTheBasketText;
        button.className = 'orange-button';
        return;
    }
    buttonSpan.textContent = itemIsOnTheBasketText;
    button.className = 'grey-button';
}

function setCounter() {
    const basketItems = getBasketItems();
    const counter = getBasketCounter();
    if (basketItems.length === 0) {
        counter.textContent = '';
        counter.hidden = true;
    } else {
        counter.textContent = basketItems.length;
        counter.hidden = false;
    }
}

function basketButtonAction(id) {
    const button = getBasketButton();
    const items = getBasketItems();
    if (items.length === 0 || !items.includes(id)) {
        addToBasket(id);
        return;
    }
    removeFromBasket(id);
}

setCounter();
setBasketButton(itemId);