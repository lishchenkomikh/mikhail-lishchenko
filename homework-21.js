// Упражнение 1
let isEmpty = function (obj) {
  let keysArray = Object.keys(obj);
  if (keysArray.length === 0) {
    return true;
  } else {
    return false;
  }
}
let a = {};
let b = { field: 'filed' };
console.log(`Пустой объект: ${isEmpty(a)}`);
console.log(`Не пустой объект: ${isEmpty(b)}`);

//Упражнение 2
let salaries = {
  John: 100000,
  Ann: 160000,
  Pete: 130000
};
function raiseSalary(perzent) {
  let newsalarias = {};
  for (let key in salaries) {
    let raise = (salaries[key] * perzent) / 100;
    newsalarias[key] = (salaries[key] + raise);
  }
  return newsalarias;
}
function getSalarySum(salaries) {
  let sum = 0;
  for (let key in salaries) {
    sum += salaries[key];
  }
  return sum;
}
let result = raiseSalary(5);
console.log(`Сумма зарплат: ${getSalarySum(result)}`);