// Упражнение 1
for (i = 0; i < 21; i=i+2) {
    console.log(i);
  }

// Упражнение 2
let sum = 0;
for (i = 0; i < 3; i = i + 1) {
  let num = +prompt("Введите число");
  if (!Number.isFinite(num)) {
    alert("Ошибка, выввели не число");
    break;
  }
  sum += num;
}
console.log(sum);

// Упражнение 3
function getNameOfMonth(month){
  switch(month) {
    case 0:
      return "январь";
    case 1:
      return "февраль";
    case 2:
      return "март";
    case 3:
      return "апрель";
    case 4:
      return "май";
    case 5:
      return "июнь";
    case 6:
      return "июль";
    case 7:
      return "август";   
    case 8:
      return "сентябрь";
    case 9:
      return "октябрь";
    case 10:
      return "ноябрь";
    case 11:
      return "декабрь";         
  }    
}
for (i = 0; i < 12; i = i + 1){
 if(i===9){
   continue;
 }
  const name = getNameOfMonth(i);
  console.log(name);
}
